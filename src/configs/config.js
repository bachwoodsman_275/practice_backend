import dotenv from "dotenv";

dotenv.config();

export default {
  database: process.env.DB_DATABASE,
  user: process.env.DB_USER,
  pass: process.env.DB_PASS,
  port: process.env.DB_port,
  host: process.env.DB_host,
  dialect: process.env.DB_DIALECT,
};
