export const responseData = (res, data, message, statusCode) => {
  res.status(statusCode).json({
    message: message,
    content: data,
    date: new Date(),
  });
};
