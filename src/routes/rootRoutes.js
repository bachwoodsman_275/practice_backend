// Tập hợp tất cả các đối tượng dô đây
import express from "express";
import { videoRoute } from "./videoRoutes.js";
import { authRoute } from "./authRoute.js";
export const rootRoute = express.Router();

rootRoute.use("/video", videoRoute);
rootRoute.use("/auth", authRoute);
