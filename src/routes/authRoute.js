import express from "express";
import { login, loginFacebook, signUp } from "../controllers/authController.js";

export const authRoute = express.Router();

authRoute.post("/sign-up", signUp);
authRoute.post("/login", login);
// authRoute.post("/login-facebook/:faceAppId", loginFacebook);
