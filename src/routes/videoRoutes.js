// Điều hướng
import {
  getCommentVideo,
  getLikeVideo,
  getVideo,
  getVideoByTypeId,
  getVideoDetailById,
  getVideoEachPage,
  getVideoType,
} from "../controllers/videoController.js";
import express from "express";
export const videoRoute = express.Router();
export const userRoute = express.Router();

// nơi quản lí API của đối tượng
videoRoute.get("/get-video", getVideo);

// get side bar title
videoRoute.get("/get-video-type", getVideoType);

// get video by typeId
videoRoute.get("/get-video-type/:typeId", getVideoByTypeId);

// get video each page
videoRoute.get("/get-video-each-page/:typeId/:pageId", getVideoEachPage);

// get video detail
videoRoute.get("/get-video-detail-by-id/:videoId", getVideoDetailById);

// get comment by video id
videoRoute.get("/get-comment-video/:videoId", getCommentVideo);

// get like by video id
videoRoute.get("/get-like-video/:videoId", getLikeVideo);
