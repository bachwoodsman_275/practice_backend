import express from "express";
import cors from "cors";
import { rootRoute } from "./routes/rootRoutes.js";

const app = express();
app.use(cors());
app.use(express.json());
app.listen(8080);
app.use(rootRoute);
// // package.json => yarn init => enter enter enter enter
