// kết nối csdl
import { Sequelize } from "sequelize";
import config from "../configs/config.js";

const sequelize = new Sequelize(config.database, config.user, config.pass, {
  port: config.port,
  host: config.host,
  dialect: config.dialect,
});

// kiem tra

// try {
//   await sequelize.authenticate();
//   console.log("Ket noi csdl thanh cong");
// } catch (error) {
//   console.log(error);
// }

export default sequelize;
