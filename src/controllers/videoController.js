import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";
import { Sequelize } from "sequelize";
import { responseData } from "../configs/response.js";

let model = initModels(sequelize); // model = Video
let Op = Sequelize.Op;
export const getVideo = async (req, res) => {
  try {
    let data = await model.video.findAll({
      include: ["user"],
    });
    responseData(res, data, "Successful", 200);
  } catch (error) {
    responseData(res, "", "Error...", 500);
  }
};

export const getVideoType = async (req, res) => {
  try {
    let data = await model.video_type.findAll();
    responseData(res, data, "Success", 200);
  } catch (error) {
    responseData(res, "", "Error...", 500);
  }
};

// get video by typeId controller
export const getVideoByTypeId = async (req, res) => {
  try {
    let { typeId } = req.params;
    let data = await model.video.findAll({
      where: {
        type_id: typeId,
      },
    });
    responseData(res, data, "Success", 200);
  } catch (error) {}
};

// get video each page
export const getVideoEachPage = async (req, res) => {
  try {
    let { pageId } = req.params;
    let { typeId } = req.params;
    if (pageId >= 1) {
      let videoInOnePage = 3;
      let videoIndexStart = (pageId - 1) * videoInOnePage;
      let data = await model.video.findAll({
        where: {
          type_id: typeId,
        },
        limit: videoInOnePage,
        offset: videoIndexStart,
      });
      let totalVideo = await model.video.count({
        where: {
          type_id: typeId,
        },
      });
      let totalPage = Math.ceil(totalVideo / videoInOnePage);
      responseData(res, { data, totalPage }, "Successful", 200);
    } else {
      responseData(res, "", "Error...", 500);
    }
  } catch (error) {
    responseData(res, "", "Error...", 500);
  }
};

// get video detail by video id
export const getVideoDetailById = async (req, res) => {
  try {
    let { videoId } = req.params;
    let data = await model.video.findOne({
      where: {
        video_id: videoId,
      },
      include: ["user"],
    });
    responseData(res, data, "Successful", 200);
  } catch (error) {
    responseData(res, "", "Error", 500);
  }
};

// get comment video
export const getCommentVideo = async (req, res) => {
  try {
    let { videoId } = req.params;
    let data = await model.video_comment.findAll({
      where: {
        video_id: videoId,
      },
      include: ["user"],
    });
    responseData(res, data, "Get Comment Successful", 200);
  } catch (error) {
    responseData(res, "", "Get Comment Unsuccessful", 500);
  }
};

// get likes video
export const getLikeVideo = async (req, res) => {
  try {
    let { videoId } = req.params;
    let data = await model.video_like.count({
      where: {
        video_id: videoId,
      },
    });
    responseData(res, data, "Get Likes Video Successfull", 200);
  } catch (error) {
    responseData(res, "", "Get Likes Video Unsuccessfull", 500);
  }
};
