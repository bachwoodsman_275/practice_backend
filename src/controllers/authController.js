import initModels from "../models/init-models.js";
import { responseData } from "../configs/response.js";
import sequelize from "./../models/connect.js";

const model = initModels(sequelize);

export const login = async (req, res) => {
  try {
    let { email, password } = req.body;
    let checkUser = await model.users.findOne({
      where: {
        email,
      },
    });
    if (checkUser) {
      if (checkUser.pass_word == password) {
        responseData(res, "token", "Login Successful", 200);
      } else {
        responseData(res, "", "Password is not valid", 404);
        return;
      }
    } else {
      responseData(res, "", "Email or Password is not valid", 404);
      return;
    }
  } catch (error) {}
};
export const signUp = async (req, res) => {
  try {
    let { full_name, email, password } = req.body;
    let checkEmail = await model.users.findOne({
      where: {
        email: email,
      },
    });
    if (checkEmail) {
      // email is not valid
      responseData(res, "", "Email is already exists", 404);
      return;
    }
    let newData = {
      full_name,
      email,
      pass_word: password,
      avatar: "",
      face_app_id: "",
      role: "user",
    };
    await model.users.create(newData);
    responseData(res, "token", "Sign Up Successful", 200);
  } catch (error) {
    responseData(res, "", "Sign Up Unsuccessful", 500);
  }
};
